package net.openfmi.moodle;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.tokensregex.MatchedExpression;
import edu.stanford.nlp.util.CoreMap;
import net.openfmi.moodle.nlp.ForumAnnotations;
import net.openfmi.moodle.nlp.MoodleEvent;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;
import java.util.stream.Collectors;

@Path("/api/smm-ner")
public class NerResource {

    private final ForumAnnotations annotator = new ForumAnnotations();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/annotate/{text}")
    public String hello(@PathParam("text") String text) {
        final List<List<MatchedExpression>> annotations = annotator.getAnnotations(text);

        final List<MoodleEvent> events = new ArrayList<>();

        for (List<MatchedExpression> matchedExpressions : annotations) {
            for (MatchedExpression me : matchedExpressions) {
                final MoodleEvent event = new MoodleEvent();

                event.setExpression(me.getText());
                event.setText("" + me.getValue().get());
                event.setOffsets(me.getCharOffsets());

                final CoreMap annotation = me.getAnnotation();
                if (annotation != null) {
                    System.out.println("matched expression tokens:" +
                            annotation.get(CoreAnnotations.TokensAnnotation.class));
                    //event.setTokens(annotation.get(CoreAnnotations.TokensAnnotation.class)));
                }

                events.add(event);
            }
        }

        return events.stream().map(MoodleEvent::toJSONString).collect(Collectors.toList()).toString();
    }
}