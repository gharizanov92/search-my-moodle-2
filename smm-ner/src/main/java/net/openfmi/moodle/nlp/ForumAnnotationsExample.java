package net.openfmi.moodle.nlp;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.tokensregex.MatchedExpression;
import edu.stanford.nlp.util.CoreMap;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.List;

public class ForumAnnotationsExample {
    public static void main(String[] args) {
        Instant.now(Clock.fixed(
                Instant.parse("2018-08-22T10:00:00Z"),
                ZoneOffset.UTC));
        // load sentences
        final String exampleSentences =
/*                "Здравейте, малкото контролно на втора група ще се проведе на 3 април(вторник) от 12:00 до 13:00 в зала 200.\n" +
                        "Другата седмица, на 15.01.2019, няма да се провежда последната лекция от дисциплината.\n" +*/
                        "Изпитът ще се проведе този петък, от 10:00 до 13:00 в Зала 210 на ФХФ.\n";// +
/*                        "Изпитът ще се проведе петък от 10:00 в зала 200 на ХФ.\n" +
                        "Изпитът ще се проведе утре в зала 200 на физическият факултет.\n";*/

        ForumAnnotations annotator = new ForumAnnotations();

        List<List<MatchedExpression>> annotations = annotator.getAnnotations(exampleSentences);

        int i = 0;

        for (List<MatchedExpression> matchedExpressions : annotations) {
            System.out.println(exampleSentences.split("\n")[i++]);
            for (MatchedExpression me : matchedExpressions) {
                System.out.println("matched expression: " + me.getText());
                System.out.println("matched expression value: " + me.getValue());
                System.out.println("matched expression char offsets: " + me.getCharOffsets());
                CoreMap annotation = me.getAnnotation();
                if (annotation != null) {
                    System.out.println("matched expression tokens:" +
                            annotation.get(CoreAnnotations.TokensAnnotation.class));
                }
            }
            System.out.println("-------------------");
        }

        System.out.println();
    }
}
