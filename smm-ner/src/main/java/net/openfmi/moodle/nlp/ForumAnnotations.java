package net.openfmi.moodle.nlp;

import edu.stanford.nlp.ling.CoreAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.tokensregex.*;
import edu.stanford.nlp.ling.tokensregex.types.Expressions;
import edu.stanford.nlp.ling.tokensregex.types.Value;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.pipeline.TokenizerAnnotator;
import edu.stanford.nlp.time.SUTime;
import edu.stanford.nlp.time.TimeAnnotations;
import edu.stanford.nlp.time.TimeAnnotator;
import edu.stanford.nlp.time.TimeExpression;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.ErasureUtils;
import edu.stanford.nlp.util.Interval;

import javax.enterprise.context.ApplicationScoped;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

/**
 * Created by ghari on 8.2.2019 г..
 */
@ApplicationScoped
public class ForumAnnotations {

    public static SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd");

    private final CoreMapExpressionExtractor<MatchedExpression> extractor;
    private final Env env;
    private Properties pipelineProperties;
    private StanfordCoreNLP pipeline;

    public ForumAnnotations() {
        pipelineProperties = new Properties();

        pipelineProperties.setProperty("annotators", "tokenize,ssplit,pos");
        pipelineProperties.setProperty("ner.applyFineGrained", "false");
        pipelineProperties.setProperty("ssplit.eolonly", "true");
        pipelineProperties.setProperty("sutime.rules", "defs.sutime.txt,english.sutime.txt");

        final Properties additionalProps = new Properties();

        pipeline = new StanfordCoreNLP(pipelineProperties);

        pipeline.addAnnotator(new TokenizerAnnotator(true));
        additionalProps.setProperty("sutime.rules", "defs.sutime.txt,english.sutime.txt");
        pipeline.addAnnotator(new TimeAnnotator("sutime", additionalProps));

        // get the rules files
        String rules = "ner.rules";

        // set up an environment with reasonable defaults
        env = TokenSequencePattern.getNewEnv();
        // set to case insensitive
        env.setDefaultStringMatchFlags(NodePattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
        env.setDefaultStringPatternFlags(Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);

        // build the CoreMapExpressionExtractor
        extractor = CoreMapExpressionExtractor.createExtractorFromFiles(env, rules);
    }

    public List<List<MatchedExpression>> getAnnotations(final String inputText) {
        return getAnnotations(inputText, /*"2019-02-15" */ sdf.format(new Date()));
    }

    public List<List<MatchedExpression>> getAnnotations(final String inputText, final String defaultDate) {

        // get sentences
        Annotation annotation = new Annotation(inputText);

        annotation.set(CoreAnnotations.DocDateAnnotation.class, defaultDate);

        pipeline.annotate(annotation);

        List<List<MatchedExpression>> result = new LinkedList<>();

        // for each sentence in the input text, run the TokensRegex pipeline
        for (CoreMap sentence : annotation.get(CoreAnnotations.SentencesAnnotation.class)) {
            List<MatchedExpression> matchedExpressions = extractor.extractExpressions(sentence);

            result.add(matchedExpressions);

            List<CoreMap> timexAnnsAll = sentence.get(TimeAnnotations.TimexAnnotations.class);
            for (CoreMap cm : timexAnnsAll) {
                List<CoreLabel> tokens = cm.get(CoreAnnotations.TokensAnnotation.class);

                Integer fromOffset = tokens.get(0).get(CoreAnnotations.CharacterOffsetBeginAnnotation.class);
                Integer toOffset = tokens.get(tokens.size() - 1).get(CoreAnnotations.CharacterOffsetEndAnnotation.class);
                SUTime.Temporal temporal = cm.get(TimeExpression.Annotation.class).getTemporal();

                MatchedExpression matchedExpression = new MatchedExpression(
                        Interval.toInterval(fromOffset, toOffset),
                        Interval.toInterval(
                                cm.get(CoreAnnotations.TokenBeginAnnotation.class),
                                cm.get(CoreAnnotations.TokenEndAnnotation.class)), // TODO: change to actual Token offset
                        new MatchedExpression.SingleAnnotationExtractor(),
                        0, 0);

                annotation.set(CoreAnnotations.DocDateAnnotation.class, temporal.toISOString());

                matchedExpression.value = new TemporalValue("Temporal", temporal);

                try {
                    Field text = MatchedExpression.class.getDeclaredField("text");
                    text.setAccessible(true);
                    text.set(matchedExpression, ((SUTime.PartialTime) temporal).getInterval().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                matchedExpressions.add(matchedExpression);
            }

        }

        return result;
    }

    public static class TemporalValue extends Expressions.TypedExpression implements Value<SUTime.Temporal> {

        public SUTime.Temporal getValue() {
            return value;
        }

        private final SUTime.Temporal value;

        public TemporalValue(String typename, SUTime.Temporal value, String... tags) {
            super(typename, tags);
            this.value = value;
        }

        @Override
        public SUTime.Temporal get() {
            return value;
        }

        @Override
        public Value evaluate(Env env, Object... args) {
            return this;
        }

        @Override
        public String toString() {
            return getType() + "(" + value + ")";
        }
    }


    public static class FmiEventTokensAnnotation implements CoreAnnotation<List<? extends CoreMap>> {
        @Override
        public Class<List<? extends CoreMap>> getType() {
            return ErasureUtils.uncheckedCast(List.class);
        }
    }

    public static class FmiEventTypeAnnotation implements CoreAnnotation<String> {
        @Override
        public Class<String> getType() {
            return ErasureUtils.uncheckedCast(String.class);
        }
    }

    public static class FmiEventValueAnnotation implements CoreAnnotation<String> {
        @Override
        public Class<String> getType() {
            return ErasureUtils.uncheckedCast(String.class);
        }
    }

}
