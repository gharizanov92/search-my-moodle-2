package net.openfmi.moodle.nlp;

import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.Interval;

public class MoodleEvent {
    private String expression;
    private String text;
    private Interval<Integer> offsets;
    private CoreMap tokens;

    public MoodleEvent() {
    }

    public MoodleEvent(String expression, String text, Interval<Integer> offsets, CoreMap tokens) {
        this.expression = expression;
        this.text = text;
        this.offsets = offsets;
        this.tokens = tokens;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Interval<Integer> getOffsets() {
        return offsets;
    }

    public void setOffsets(Interval<Integer> offsets) {
        this.offsets = offsets;
    }

    public CoreMap getTokens() {
        return tokens;
    }

    public void setTokens(CoreMap tokens) {
        this.tokens = tokens;
    }

    public String toJSONString() {
        return "{" +
                "\"expression\":\"" + expression + '\"' +
                ",\"text\":\"" + text + '\"' +
                ",\"offsets\":\"" + offsets + '\"' +
                ",\"tokens\":\"" + tokens + '\"' +
                '}';
    }
}
