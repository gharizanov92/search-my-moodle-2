package net.openfmi.moodle.nlp;

import edu.stanford.nlp.ling.tokensregex.CoreMapExpressionExtractor;
import edu.stanford.nlp.ling.tokensregex.Env;
import edu.stanford.nlp.ling.tokensregex.NodePattern;
import edu.stanford.nlp.ling.tokensregex.TokenSequencePattern;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

import javax.enterprise.context.ApplicationScoped;
import java.util.Properties;
import java.util.regex.Pattern;

@ApplicationScoped
public class Pipeline {

    private static Properties properties;
    private static String propertiesName = "tokenize, ssplit, pos, lemma, ner";
    private static StanfordCoreNLP stanfordCoreNLP;
    // get the rules files
    private static final String rules = "ner.rules";

    // set up an environment with reasonable defaults
    private static Env env = TokenSequencePattern.getNewEnv();

    private Pipeline() {

    }

    static {
        properties = new Properties();
        properties.setProperty("annotators", propertiesName);
        properties.setProperty("annotators", "tokenize,ssplit,pos");
        properties.setProperty("ner.applyFineGrained", "false");
        properties.setProperty("ssplit.eolonly", "true");
        properties.put("sutime.rules", "defs.sutime.txt,english.sutime.txt");

        // set to case insensitive
        env.setDefaultStringMatchFlags(NodePattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
        env.setDefaultStringPatternFlags(Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
    }

    // @Bean(name = "stanfordCoreNLP")
    public static StanfordCoreNLP getInstance() {
        if (stanfordCoreNLP == null) {
            stanfordCoreNLP = new StanfordCoreNLP(properties);
        }
        return stanfordCoreNLP;
    }

    // @Bean(name = "extractor")
    public static CoreMapExpressionExtractor getExtractor() {
        return CoreMapExpressionExtractor.createExtractorFromFiles(env, rules);
    }
}
