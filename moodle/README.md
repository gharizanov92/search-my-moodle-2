# Local Moodle Instance Setup
## Requirements
* Docker 1.10.0+
* Docker-Compose 1.6.0+
* Kubernetes 1.12+
* Helm 2.11+ or Helm 3.0-beta3+
* PV provisioner support in the underlying infrastructure
* ReadWriteMany volumes for deployment scaling
## Running Moodle
### Docker
* run ```docker-compose up -d```
* Go to http://localhost:9090/admin/category.php?category=mobileapp and check `Enable web services for mobile devices`
* Go to http://localhost:9090/admin/settings.php?section=optionalsubsystems and check `Enable web services` and `Enable RSS feeds`
* Add some courses and some users and test
