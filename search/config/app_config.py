import os

from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
# db = LocalProxy(lambda: _get_db())


def init(app):
    db.init_app(app)


class Config(object):
    DEBUG = False
    TESTING = False
    BABEL_DEFAULT_LOCALE = 'bg_BG'
    SECRET_KEY = '02ARhZB8A2bK4L4ipOij'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    MOODLE_URL = ''
    FLASK_RUN_HOST = '0.0.0.0'
    TF_IDF_MODEL = './tmp/tf_idf.model'
    TF_IDF_VECTORIZER = './tmp/tf_idf.vectorizer'

    def init_db(self, app):
        pass


class ProductionConfig(Config):
    # SQLALCHEMY_DATABASE_URI = 'mysql://user@localhost/foo'
    SQLALCHEMY_DATABASE_URI = 'sqlite:///db.sqlite'
    MOODLE_URL = 'https://learn.fmi.uni-sofia.bg'

    def init_db(self, app):
        db.init_app(app)


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///db.sqlite'
    MOODLE_URL = 'http://moodle.harizanov.info'
    FILES_SERVICE_URL = 'http://0.0.0.0:5001'

    def init_db(self, app):
        db.init_app(app)


config = None

if os.environ['FLASK_ENV'] == 'production':
    config = ProductionConfig()
else:
    config = DevelopmentConfig()


def main():
    pass


if __name__ == "__main__":
    pass
