#!/usr/local/bin/python
# -*- coding: utf-8 -*-
'''
Algorithm author: Preslav Nakov <nakov@cs.berkeley.edu>, UC Berkeley
Paper: BulStem: Inflectional Stemmer For Bulgarian http://people.ischool.berkeley.edu/~nakov/bulstem/
Description: Stems a text file
'''

import pickle
import re


class BulgarianStemmer(object):
    def __init__(self):
        self.filename = './search/stemmer/rules/stem_rules_context_1.txt'
        self.min_rule_freq = 2
        self.min_wrod_len = 3
        self.re_bg_vowels = re.compile(u"[аъоуеияю]")

        try:
            self.stemmingRules = pickle.load(open(
                './search/stemmer/rules/StemmingRules-MinFreq-' +
                str(self.min_rule_freq) + '.pickle', 'rb'))
        except:
            self.stemmingRules = self.fetch_the_rules()

    def __call__(self, word):
        return self.stem(word)

    def fetch_the_rules(self):
        'Read the rules and load them into dictionary'
        import codecs

        re_empty_line = re.compile('^\s*$')
        re_rule_line = re.compile(u"([а-я-]+) ==> ([a-я-]+) (\d+)", re.U)
        StemmingRules = {}

        for rule in codecs.open(self.filename, 'r', 'WINDOWS-1251').readlines():
            if re_empty_line.match(rule):
                continue

            'Break the rule line in three parts match(1) - reduce(2) - probability(3)'
            rule_parts = re_rule_line.match(rule)

            if rule_parts is not None:
                if int(rule_parts.group(3)) > self.min_rule_freq:

                    'Build a dictionary indexed by the length of the match'
                    match_len = len(rule_parts.group(1))
                    try:
                        StemmingRules[match_len][rule_parts.group(1)] = rule_parts.group(2)
                    except KeyError:
                        StemmingRules[match_len] = {}
                        StemmingRules[match_len][rule_parts.group(1)] = rule_parts.group(2)

            else:
                print
                "Bad stemming rule:", rule.encode('utf-8')
                continue

        'Using a pickle would be faster'
        pickle.dump(StemmingRules, open(
            './search/stemmer/rules/StemmingRules-MinFreq-' + str(
                self.min_rule_freq) + '.pickle', 'wb'))

        return StemmingRules

    def stem(self, word):
        'Stem the word'

        'Do not stem short words'
        wordLen = len(word)
        if wordLen <= self.min_wrod_len:
            return word

        'If no bulgarian vowel - no valid word'
        if not self.re_bg_vowels.match(word):
            return word

        'Convert to lower case in order to compare it easy'
        word = word.lower()

        'Start from the minimal meaningful word'
        c = self.min_wrod_len
        for _ in word:
            'Reduce the word from the beginning towards the end'
            stem = word[c:wordLen]

            'Calculate the reminding symbols for better search'
            word_reminder = wordLen - c

            'Check if there is a stem matching the reminder of the word'
            if stem in self.stemmingRules[word_reminder]:
                'Return stemmed word'
                return word[:c] + self.stemmingRules[word_reminder][stem]
                break
            else:
                c += 1

        'Always return something'
        return word
