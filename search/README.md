# Search My Moodle
### OS Dependencies
Debian, Ubuntu, and friends:
```
sudo apt-get update
sudo apt-get install build-essential libpoppler-cpp-dev pkg-config python-dev
```

Fedora, Red Hat, and friends:
```
sudo yum install gcc-c++ pkgconfig poppler-cpp-devel python-devel redhat-rpm-config
```

macOS:
```
brew install pkg-config poppler
CPPFLAGS="-std=c++11" pip install pdftotext
```

### Install Python dependencies
```
$ pip install -r requirements.txt
```
### Create the database
```python
>>> from search.config.app_config import db
>>> from search.app import create_app
>>> db.create_all(app=create_app())
```
### (Optionally) Run a Moodle instance locally
[See how](moodle/README.md)
### Running the app
```
$ export FLASK_ENV=development
$ flask run
```