from flask_table import Table, Col, DateCol, LinkCol
from datetime import date

import sys

from flask_table.html import element

sys.path.append('../')


class BulmaTable(Table):
    classes = ['table', 'is-striped', 'is-hoverable', 'is-fullwidth']

    def sort_url(self, col_id, reverse=False):
        pass


class TimestampDateCol(DateCol):
    """Format the content as a date from timestamp

    """

    def __init__(self, name, date_format='short', **kwargs):
        super(TimestampDateCol, self).__init__(name, date_format, **kwargs)

    def td_format(self, content):
        return date.fromtimestamp(content)


class MoodleHrefCol(Col):

    def __init__(self, name, text="Свали", attr=None, attr_list=None, **kwargs):
        super(MoodleHrefCol, self).__init__(
            name,
            attr=attr,
            attr_list=attr_list,
            **kwargs)
        self.text = text

    def get_attr_list(self, attr):
        return super(MoodleHrefCol, self).get_attr_list(None)

    def td_contents(self, item, attr_list):
        attrs = dict(href=self.from_attr_list(item, attr_list).replace("/webservice", ""))
        return element('a', attrs=attrs, content='<i class="fa fa-download" aria-hidden="true"></i> ' + item['id'],
                       escape_content=False)


class ForumPostCol(Col):

    def __init__(self, name, **kwargs):
        super(ForumPostCol, self).__init__(name, **kwargs)

    def td_contents(self, item, attr_list):
        attrs = dict(href=self.from_attr_list(item, attr_list).replace("/webservice", ""))
        return element('p', attrs=attrs, content=self.from_attr_list(item, attr_list),
                       escape_content=False)


class CourseTable(BulmaTable):
    allow_sort = False

    shortname = Col('Име')
    fullname = Col('Пълно Име')
    startdate = TimestampDateCol('Начална Дата', date_format='medium')
    enddate = TimestampDateCol('Крайна Дата', date_format='medium')
    forums = LinkCol('', 'main.forums', text_fallback='Форуми', url_kwargs=dict(id='id'),
                     anchor_attrs={'class': 'link'})
    attachments = LinkCol('', 'main.attachments', text_fallback='Файлове', url_kwargs=dict(id='id'),
                          anchor_attrs={'class': 'link'})
    assignments = LinkCol('', 'main.assignments', text_fallback='Задания', url_kwargs=dict(id='id'),
                          anchor_attrs={'class': 'link'})


class AttachmentTable(BulmaTable):
    allow_sort = False

    fileurl = MoodleHrefCol('', attr='fileurl')
    filename = Col('Име')
    author = Col('Автор')


class ForumTable(BulmaTable):
    allow_sort = False

    name = Col('Име')
    numdiscussions = Col('Дискусии')
    attachments = LinkCol('', 'main.discussions', text_fallback='Дискусии', url_kwargs=dict(id='id'),
                          anchor_attrs={'class': 'link'})


class DiscussionTable(BulmaTable):
    allow_sort = False

    userfullname = Col('Автор')
    name = Col('subject')
    numreplies = Col('Отговори')
    posts = LinkCol('', 'main.discussion_posts', text_fallback='Отговори', url_kwargs=dict(id='discussion'),
                    anchor_attrs={'class': 'link'})


class PostsTable(BulmaTable):
    allow_sort = False

    userfullname = Col('Автор')
    subject = Col('Тема')
    message = ForumPostCol('Съобщение')
