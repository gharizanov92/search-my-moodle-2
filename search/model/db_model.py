from flask_login import UserMixin
from sqlalchemy import Table
from search.config.app_config import db

# association_table = Table('association', db.Base.metadata,
#     db.Column('left_id', db.Integer, db.ForeignKey('left.id')),
#     db.Column('right_id', db.Integer, db.ForeignKey('right.id'))
# )


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(1000))
    token = db.Column(db.String(1000))
    last_login = db.Column(db.DateTime)


class Attachment(db.Model):
    id = db.Column(db.String, primary_key=True)
