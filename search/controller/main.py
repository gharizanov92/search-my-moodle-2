from flask import Blueprint, redirect, render_template, url_for, request
from flask_login import login_required, current_user

from search.model.frontend_components import CourseTable, AttachmentTable, ForumTable, DiscussionTable, PostsTable
from search.service.moodle_client import get_user_enrolled_courses, get_course_file_attachments, get_course_forums, \
    get_forum_discussions, get_forum_discussion_posts

main = Blueprint('main', __name__)


@main.route('/')
def index():
    if current_user.is_authenticated:
        return redirect(url_for('main.courses'))
    return render_template('index.html')


@main.route('/courses')
@login_required
def courses():
    return render_template('courses.html',
                           courses=CourseTable(get_user_enrolled_courses(current_user.token, current_user.id)))


@main.route('/forums')
@login_required
def forums():
    return render_template('forums.html',
                           forums=ForumTable(get_course_forums(current_user.token, request.args.get('id'))))


@main.route('/discussions')
@login_required
def discussions():
    forum_id = request.args.get('id')
    if forum_id:
        discussions = get_forum_discussions(current_user.token, forum_id)
    else:
        return render_template('discussions.html', discussions='Моля изберете курс')

    return render_template('discussions.html',
                           discussions=DiscussionTable(discussions['discussions']))


@main.route('/discussion/posts')
@login_required
def discussion_posts():
    discussion_id = request.args.get('id')
    if discussion_id:
        posts = get_forum_discussion_posts(current_user.token, discussion_id, sort_direction='ASC')
    else:
        return render_template('posts.html', posts='Моля изберете курс')

    return render_template('posts.html',
                           posts=PostsTable(posts['posts']))


@main.route('/attachments')
@login_required
def attachments():
    course_id = request.args.get('id')
    if course_id:
        attachments = get_course_file_attachments(current_user.token, course_id)
    else:
        # courses = fetch_courses(current_user)
        # attachments = reduce(list.__add__, list(map(lambda course: get_course_file_attachments(current_user.token, course['id']), courses)))
        return render_template('attachments.html', attachnents='Моля изберете курс')

    return render_template('attachments.html', attachnents=AttachmentTable(attachments))


@main.route('/assignments')
@login_required
def assignments():
    pass
