from flask import Blueprint, redirect, render_template, url_for, request
from flask_login import login_required, current_user

moodle = Blueprint('moodle', __name__)


@moodle.route('/attachment/download')
@login_required
def download_attachment():
    pass
