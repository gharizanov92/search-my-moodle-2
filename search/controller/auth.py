from flask import Blueprint, render_template, redirect, url_for, request, flash
from flask_login import login_user, logout_user, login_required
from search.model.db_model import User
from search.config.app_config import db
from search.service.moodle_client import get_token, get_userdata
from datetime import datetime
from search.service.indexer import index
from search.service.moodle_client import get_all_user_attachments_ids

auth = Blueprint('auth', __name__)


@auth.route('/login')
def login():
    return render_template('login.html')


@auth.route('/login', methods=['POST'])
def login_post():
    username = request.form.get('username')
    password = request.form.get('password')
    remember = True if request.form.get('remember') else False

    user = User.query.filter_by(name=username).first()

    if not user:
        resp = get_token(username, password)
        if resp.status_code == 200:
            token = resp.json()['token']
            user_data = get_userdata(token).json()

            user = User(name=username, token=token, id=user_data['userid'], last_login=datetime.now())
            db.session.add(user)
            db.session.commit()
        else:
            flash('Не съществува такъв потребител')
            return redirect(url_for('auth.login'))  # reload the page

    login_user(user, remember=remember)
    index.update_index(user.token, get_all_user_attachments_ids(user.token, user.id))
    print(index.recommend_document("Персептрон и дървета"))
    return redirect(url_for('main.courses'))


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('main.index'))
