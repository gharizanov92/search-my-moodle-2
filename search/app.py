from flask import Flask
from flask_login import LoginManager

from search.config.app_config import db
from search.model.db_model import User
from search.controller.auth import auth as auth_blueprint
from search.controller.main import main as main_blueprint
from search.controller.moodle import moodle as moodle_blueprint
from search.config import app_config

config = app_config.config


def create_app():
    app = Flask(__name__)
    app.config.from_object(config)
    config.init_db(app)
    app_config.init(app)

    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_user(user_id):
        # since the user_id is just the primary key of our user table, use it in the query for the user
        return User.query.get(int(user_id))

    app.register_blueprint(auth_blueprint)
    app.register_blueprint(main_blueprint)
    app.register_blueprint(moodle_blueprint)

    return app


db.create_all(app=create_app())


def main():
    pass


if __name__ == "__main__":
    from waitress import serve
    serve(create_app(), host="0.0.0.0", port=5000)
