import os.path
import pickle
import re
import statistics

import numpy as np
import pandas as pd
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import ToktokTokenizer
from scipy.sparse import dia_matrix
from sklearn.feature_extraction.text import TfidfVectorizer, HashingVectorizer
from sklearn.metrics.pairwise import cosine_similarity

from search.config.app_config import config
from search.service.moodle_client import file_service_get_attachment_content, get_token, get_userdata, \
    get_all_user_attachments_ids
from search.stemmer.bulgarianStemmer import BulgarianStemmer
from search.config.app_config import db
from search.model.db_model import Attachment

from scipy.sparse import vstack as vstack_sparse_matrices
from scipy.sparse import csr_matrix
from scipy.sparse import hstack
from scipy.sparse import vstack


# monkey patch TfidfVectorizer to support updating index
def partial_fit(self, x):
    max_idx = max(self.vocabulary_.values())
    for a in x:
        # update vocabulary_
        if self.lowercase: a = a.lower()
        tokens = re.findall(self.token_pattern, a)
        for w in tokens:
            if w not in self.vocabulary_:
                max_idx += 1
                self.vocabulary_[w] = max_idx

        # update idf_
        df = (self.n_docs + self.smooth_idf) / np.exp(self.idf_ - 1) - self.smooth_idf
        self.n_docs += 1
        df.resize(len(self.vocabulary_), refcheck=False)
        for w in tokens:
            df[self.vocabulary_[w]] += 1
        idf = np.log((self.n_docs + self.smooth_idf) / (df + self.smooth_idf)) + 1
        self._tfidf._idf_diag = dia_matrix((idf, 0), shape=(len(idf), len(idf)))


TfidfVectorizer.partial_fit = partial_fit


class Indexer(object):

    def __init__(self):
        self.vectorize = None
        self.model = None
        self.toktok_tok = ToktokTokenizer()
        self.stop_words = stopwords.words("english")
        self.lemmatizer = WordNetLemmatizer()
        self.bulstem = BulgarianStemmer()

        if os.path.exists(config.TF_IDF_MODEL):
            self.model = pickle.load(open(config.TF_IDF_MODEL, 'rb'))
            self.vectorize = pickle.load(open(config.TF_IDF_VECTORIZER, 'rb'))

    def clean_text(self, text):
        return [
            self.lemmatizer.lemmatize(word.lower()) and
            self.bulstem.stem(word.lower())

            for word in self.toktok_tok.tokenize(text)
            if word.isalpha()
            and not word.lower() in self.stop_words
            and len(word) >= 3
        ]

    def update_index(self, token, attchment_ids):
        df = pd.DataFrame(columns=['attchment_id', 'text_clean'])
        new_attachments = []

        existing_attachment_ids = list(map(lambda a: a.id, db.session.query(Attachment).filter(Attachment.id.in_(attchment_ids)).all()))
        new_attachment_ids = list(filter(lambda id: id not in existing_attachment_ids, attchment_ids))

        n_docs = db.session.query(Attachment).count()

        for attchment_id in new_attachment_ids:
            new_attachments.append(Attachment(id=attchment_id))

        # add any new attachments to the DB
        if new_attachments.__len__() > 0:
            for attchment_id in attchment_ids:
                text = file_service_get_attachment_content(token, attchment_id)
                if text == []:
                    continue

                df = df.append({'attchment_id': attchment_id, 'text_clean': text}, ignore_index=True)

            if self.model is None:
                self.vectorize = TfidfVectorizer()
                self.vectorize.fit(df["text_clean"])
                self.model = self.vectorize.transform(df["text_clean"])
            else:
                self.vectorize.n_docs = n_docs
                self.vectorize.partial_fit(df["text_clean"])
                self.model = self.vectorize.transform(df['text_clean'])

            with open(config.TF_IDF_VECTORIZER, 'wb') as fin:
                pickle.dump(self.vectorize, fin)
            # self.model = self.vectorize.fit_transform(df["text_clean"])
            with open(config.TF_IDF_MODEL, 'wb') as fin:
                pickle.dump(self.model, fin)

            # Only save to DB if everything else has passed
            db.session.add_all(new_attachments)
            db.session.commit()

    def recommend_document(self, query, top_n=10):
        vec_search = self.vectorize.transform(self.clean_text(query))
        similarity_matrix = cosine_similarity(self.model, vec_search)
        similarity = []
        for phrase in similarity_matrix:
            similarity.append(statistics.mean(phrase))

        df = pd.DataFrame(columns=['attchment_id', 'similarity'])
        df['attchment_id'] = list(map(lambda a: a.id, db.session.query(Attachment).all()))
        df['similarity'] = similarity
        return df.sort_values('similarity', ascending=False)[df['similarity'] > 0]

index = Indexer()
