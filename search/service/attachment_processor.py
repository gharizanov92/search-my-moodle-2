import os
#import pdftotext
from urllib.request import urlretrieve


def download_attachment(attachment, token):
    filename = '../tmp' + attachment['fileurl'][54:][:-16]
    url = attachment['fileurl'] + "&token=" + token

    os.makedirs(os.path.dirname(filename), exist_ok=True)

    urlretrieve(url, filename)


# def get_remote_pdf_content(attachment, token):
#     url = attachment['fileurl'] + "&token=" + token
#     remote_file = urlopen(url).read()
#     memory_file = io.BytesIO(remote_file)
#     content = []
#
#     # pdf = PyPDF2.PdfFileReader(memory_file)
#
#     pdf = pdftotext.PDF(memory_file)
#
#     [content.append(page) for page in pdf]
#     # [content.append(page.extractText()) for page in pdf.pages]
#
#     return content
