from functools import reduce

import requests

from search.config.app_config import config
from search.service.html_utils import html_to_text

moodle_url = config.MOODLE_URL
files_service_url = config.FILES_SERVICE_URL


def get_token(username, password):
    return requests.get(
        moodle_url + '/login/token.php?username=' + username + '&password=' + password + '&service=moodle_mobile_app')


def get_userdata(token):
    return requests.get(moodle_ws_query(token, 'core_webservice_get_site_info'))


def get_user_enrolled_courses(token, user_moodle_id):
    resp = requests.get(moodle_ws_query(token, 'core_enrol_get_users_courses', {'userid': user_moodle_id}))

    if resp.status_code != 200:
        return []

    return resp.json()


def get_course_content(token, course_id):
    resp = requests.get(moodle_ws_query(token, 'core_course_get_contents', {'courseid': course_id}))

    if (resp.status_code != 200):
        return []

    return resp.json()


def get_course_file_attachments(token, course_id):
    course_content = get_course_content(token, course_id)

    modules = _get_modules_from_course_content(course_id, course_content)
    files = _get_files_from_modules(modules)

    return files


def get_all_user_attachments_ids(token, userId):
    course_ids = [attachment['id'] for attachment in get_user_enrolled_courses(token, userId)]
    all_attachments = []
    for course_id in course_ids:
        for attachment in get_course_file_attachments(token, course_id):
            all_attachments.append(attachment["id"])

    return all_attachments


def _get_modules_from_course_content(course_id, course_content):
    content_with_modules = list(filter(lambda cnt: 'modules' in cnt, course_content))
    modules = []
    for content in content_with_modules:
        content_modules = content['modules']
        del content['modules']
        for module in content_modules:
            module['section'] = content
            content['course_id'] = course_id
            modules.append(module)

    return modules


def _get_files_from_modules(modules):
    modules_with_files = list(filter(lambda cnt: 'contents' in cnt, modules))
    files = []
    for content in modules_with_files:
        content_modules = content['contents']
        del content['contents']
        for num, file in enumerate(content_modules, start=0):
            file['module'] = content
            module_id = content['id']
            section_id = content['section']['id']
            course_id = content['section']['course_id']
            file['id'] = '{0!s}_{1!s}_{2!s}_{3!s}'.format(course_id, section_id, module_id, num)
            files.append(file)

    return files


def get_course_assignments(token, course_id=None, capabilities=[], include_not_enrolled_courses=False):
    """

    :param token:
    :param course_id: An optional array of course ids. If provided only assignments within the given course
     will be returned. If the user is not enrolled in or can't view a given course
     a warning will be generated and returned.
    :return: An array of courses and warnings.
    """
    resp = requests.get(moodle_ws_query(token, 'mod_assign_get_assignments', {'courseids[0]': course_id}))

    if (resp.status_code != 200):
        return []

    return resp.json()


def get_course_forums(token, course_id=None):
    """

    :param token:
    :param course_id:
    :return: Returns a list of forums in a provided list of courses,
    if no list is provided all forums that the user can view will be returned.
    """
    course_ids = {'courseids[0]': course_id} if course_id != None else {}
    resp = requests.get(moodle_ws_query(token, 'mod_forum_get_forums_by_courses', course_ids))

    if (resp.status_code != 200):
        return []

    return resp.json()


def get_forum_discussion_posts(token, discussion_id, sort_by='created', sort_direction='DESC'):
    """
    :param sort_by -- can be 'id', 'created' or 'modified'
    :param sort_direction -- ASC or DESC
    :return: Returns the posts in a discussion
    """
    resp = requests.get(moodle_ws_query(token, 'mod_forum_get_forum_discussion_posts', {'discussionid': discussion_id,
                                                                                        'sortby': sort_by,
                                                                                        'sortdirection': sort_direction
                                                                                        }))

    if resp.status_code != 200:
        return []

    return resp.json()


def get_forum_discussion_posts_tuples(token, discussion_id, sort_by='created', sort_direction='DESC'):
    """
    :param sort_by -- can be 'id', 'created' or 'modified'
    :param sort_direction -- ASC or DESC
    :return: Returns the posts in a discussion as tuples (subject, message)
    """

    posts = requests.get(moodle_ws_query(token, 'mod_forum_get_forum_discussion_posts', {'discussionid': discussion_id,
                                                                                        'sortby': sort_by,
                                                                                        'sortdirection': sort_direction
                                                                                        }))

    result = []

    for post in posts:
        result.append((post['subject'], html_to_text(post['message'])))

    return result


def get_forum_discussions(token, forum_id, page=0):
    """

    :param token: moodle ws token
    :param forum_id: the forum instance id
    :param sort_order: The sort order
    :param page: page number
    :param per_page: items per page
    :param group_id: the user course group
    :return:
    """
    # mod_forum_get_forum_discussions_parameters
    resp = requests.get(moodle_ws_query(token, 'mod_forum_get_forum_discussions',
                                        {'forumid': forum_id,
                                         'page': page
                                         }))

    if resp.status_code != 200:
        return []

    return resp.json()


def moodle_ws_query(wstoken, wsfunction, args={}):
    req = moodle_url + '/webservice/rest/server.php?wstoken=' + wstoken \
          + '&wsfunction=' + wsfunction + '&moodlewsrestformat=json'

    for key, value in args.items():
        req += ("&{0}={1}".format(key, value.__str__()))

    return req


def file_service_get_attachment_content(token, attchment_id, args={}):
    req = files_service_url + '/files/download?token=' + token \
          + '&id=' +  attchment_id

    for key, value in args.items():
        req += ("&{0}={1}".format(key, value.__str__()))

    resp = requests.get(req)
    if resp.status_code != 200:
        return []

    return resp.json()


def get_all_forum_posts_tuples(token):
    posts = reduce(list.__add__, map(lambda d: get_forum_discussion_posts(token, d['discussion'])['posts'],
                                     reduce(list.__add__,
                                            map(lambda f: get_forum_discussions(token, f['id'])['discussions'],
                                                get_course_forums(token)))))

    result = []

    for post in posts:
        result.append((post['subject'], html_to_text(post['message'])))

    return result



# functions = userdata.json()['functions']
# forums = get_course_forums(token, 3)
# all_posts = get_all_forum_posts_tuples(token)
#
# forum_discussions = get_forum_discussions(token, 3)['discussions']
# forum_discussion_posts = get_forum_discussion_posts(token, 2)['posts']
#
# get_remote_pdf_content_from_id(token, '3_8_5_0')
#
# print()
# ws = invoke_moodle_ws(token, 'mod_assign_get_assignments', {'courseids[0]': '5'})


# user_data = get_userdata(token)
# attachments = get_course_file_attachments(token, 3)
# download_attachment(attachments[0], token)
# get_remote_pdf_content(attachments[0], token)
#
# assignments = get_course_assignments(token, 3)
#
# print()

#
# print(course_content)
pass
