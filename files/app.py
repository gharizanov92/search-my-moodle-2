import io
import os
import pdftotext
from urllib.request import urlopen

import requests
from flask import Flask, request, jsonify

app = Flask(__name__)

if os.environ['FLASK_ENV'] == 'production':
    moodle_url = 'https://learn.fmi.uni-sofia.bg'
else:
    moodle_url = 'http://moodle.harizanov.info'


def get_remote_pdf_content_from_id(token, id):
    course_id, section_id, module_id, num = id.split("_")

    req = moodle_url + '/webservice/rest/server.php?wstoken=' + token \
          + '&wsfunction=core_course_get_contents&moodlewsrestformat=json&courseid=' + course_id

    resp = requests.get(req)

    if resp.status_code != 200:
        return ''

    content = resp.json()

    section = [x for x in content if x['id'].__str__() == section_id][0]
    module = [x for x in section['modules'] if x['id'].__str__() == module_id][0]
    attachment = module['contents'][int(num)]

    url = attachment['fileurl'] + "&token=" + token
    remote_file = urlopen(url).read()
    memory_file = io.BytesIO(remote_file)
    text_content = []

    pdf = pdftotext.PDF(memory_file)
    [text_content.append(page) for page in pdf]

    return "".join(text_content)


@app.route("/files/download")
def hello():
    token = request.args.get('token')
    id = request.args.get('id')
    return jsonify(get_remote_pdf_content_from_id(token, id))


if __name__ == "__main__":
    from waitress import serve
    serve(app, host="0.0.0.0", port=5001)
